import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import libaryReducer from '../features/libary/libarySlice';

export const store = configureStore({
  reducer: {
    libary: libaryReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
