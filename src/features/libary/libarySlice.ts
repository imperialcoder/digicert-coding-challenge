import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { fetchLibary, fetchGenres, Book } from './libaryAPI';

export interface LibaryState {
    books: Array<Book>
    genres: Array<string>
    status: 'idle' | 'loading' | 'failed';
}

const initialState: LibaryState = {
    books: [],
    genres: [],
    status: 'idle',
};

export const fetchLibaryAsync = createAsyncThunk(
    'libary/fetchLibary',
    async () => {
        const response = await fetchLibary();
        return response.data;
    }
);

export const fetchGenresAsync = createAsyncThunk(
    'libary/fetchGenres',
    async () => {
        const response = await fetchGenres();
        return response.data;
    }
);

export const libarySlice = createSlice({
    name: 'libary',
    initialState,
    reducers: {
        addBook(state, action: PayloadAction<Book>) {
            state.books.push(action.payload);
        },
        removeBook(state, action: PayloadAction<Book>) {
            const { id } = action.payload;
            state.books = state.books.filter(book => book.id !== id);
        },
        editBook(state, action: PayloadAction<Book>) {
            const index = state.books
                .map(book => book.id)
                .indexOf(action.payload.id);

            state.books[index] = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchLibaryAsync.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchLibaryAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                state.books = action.payload;
            })
            .addCase(fetchLibaryAsync.rejected, (state) => {
                state.status = 'failed';
            })
            .addCase(fetchGenresAsync.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchGenresAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                state.genres = action.payload;
            })
            .addCase(fetchGenresAsync.rejected, (state) => {
                state.status = 'failed';
            });
    },
});

export const { addBook, removeBook, editBook } = libarySlice.actions
export default libarySlice.reducer;

