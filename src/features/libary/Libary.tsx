import { useEffect, useState } from "react";
import { Book } from './libaryAPI'

import '../../styles/Libary.css'

import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { 
    addBook,
    editBook,
    removeBook,
    fetchLibaryAsync, 
    fetchGenresAsync,
} from "./libarySlice";

export function Libary() {
    const dispatch = useAppDispatch();

    const [selectedGenre, setSelectedGenre] = useState('all');
    const [bookForm, setBookForm] = useState<Partial<Book>>();
    const [addingForm, setAddingForm] = useState<Partial<Book>>();

    const { books, genres, status } = useAppSelector(state => ({
        books: state.libary.books,
        genres: state.libary.genres,
        status: state.libary.status,
    }));

    useEffect(() => {
        dispatch(fetchGenresAsync());
        dispatch(fetchLibaryAsync());
    }, [dispatch]);

    const isBookSelected = (book: Book) => 
        bookForm?.id === book.id;

    const isAdding = () =>
        addingForm !== undefined;

    function getBooks() {
        if (selectedGenre == 'all') {
            return books;
        } else {
            return books.filter(book => book.genre.includes(selectedGenre));
        }
    }

    function genreSelectEventHandler(event: React.ChangeEvent<HTMLSelectElement>) {
        setSelectedGenre(event.target.value);
    }

    function bookFormInputHandler(event: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        setBookForm({
            ...bookForm,
            [event.target.id]: event.target.value 
        });
    }

    function bookFormSubmitHandler(type: 'edit' | 'save' | 'remove', book: Book) {
        switch (type) {
            case 'edit':
                setBookForm(book);
                break;
            case 'save':
                dispatch(editBook({...book, ...bookForm }));
                setBookForm(undefined);
                break;
            case 'remove':
                dispatch(removeBook(book));
                break;
        }
    }

    function addingFormEventHandler(event: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        setAddingForm({
            ...addingForm, 
            [event.target.id]: event.target.value 
        });
    }

    function addingFormButtonHandler(type: 'add' | 'submit') {
        const result = Object.assign({
                    id: Date.now(),
                    name: '',
                    genre: '',
                    author: ''
        }, addingForm);
        switch (type) {
            case 'add':
                setAddingForm({}); break;
            case 'submit':
                console.log(result);
                dispatch(addBook(result));
                setAddingForm(undefined);
                break;
        }
    }

    return (
        <div className="Libary">
            <select 
                disabled={status === 'loading'} 
                onChange={genreSelectEventHandler}
                defaultValue='all'
            >
                <option value='all'>All Genres</option>
                {genres.map((genre, index) => (
                    <option key={`${genre}-${index}`} value={genre}>
                        {genre}
                    </option>
                ))}
            </select>
            {status === 'loading' &&
                <p>Loading...</p>
            }
            <ul>
                {status === 'idle' && getBooks().map((book) => (
                    <li key={book.id}>
                        <label htmlFor="name">Title</label>
                        <input 
                            id="name"
                            type="text" 
                            disabled={!isBookSelected(book)} 
                            defaultValue={book.name}
                            onChange={bookFormInputHandler} 
                            />
                        <label htmlFor="genre">Genre</label>
                        <select 
                            disabled={!isBookSelected(book)}
                            defaultValue={book.genre}
                        >
                            {genres.map((genre) => (
                                <option key={genre} value={genre}>
                                    {genre}
                                </option>
                            ))}
                        </select>
                        <label htmlFor="author">Author</label>
                        <input 
                            id="author"
                            type="text" 
                            disabled={!isBookSelected(book)} 
                            defaultValue={book.author}
                            onChange={bookFormInputHandler} 
                            />
                        <button 
                            disabled={bookForm !== undefined && !isBookSelected(book)}
                            onClick={() => bookFormSubmitHandler(isBookSelected(book) ? "save" : "edit", book)}
                        >
                            {isBookSelected(book) ? "save" : "edit" }
                        </button>
                        <button onClick={() => bookFormSubmitHandler('remove', book)}>
                            remove
                        </button>
                    </li>
                ))}
            </ul>
            {isAdding() && (
                <div className="adding-dialog">
                    <label htmlFor="name">Title</label>
                    <input 
                        id="name"
                        type="text" 
                        onChange={addingFormEventHandler} 
                        />
                    <label htmlFor="genre">Genre</label>
                    <select id="genre" defaultValue="" onChange={addingFormEventHandler}>
                        <option disabled value=''>Select a Genre</option>
                        {genres.map((genre, index) => (
                            <option key={`${genre}-${index}`} value={genre}>
                                {genre}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="author">Author</label>
                    <input 
                        id="author"
                        type="text" 
                        onChange={addingFormEventHandler} 
                        />
                </div>
            )}
            <button onClick={() => addingFormButtonHandler(isAdding() ? 'submit' : 'add')}>
                {isAdding() ? 'Submit' : 'Add Book'}
            </button>
        </div>
    );
}
