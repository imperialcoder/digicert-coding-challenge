export interface Book {
    id: number,
    name: string,
    genre: string,
    author: string,
}

export function fetchLibary() {
    return new Promise<{ data: Array<Book> }>((resolve) =>
        setTimeout(() => resolve({ data: dummyLibary }), 500));
}

export function fetchGenres() {
    return new Promise<{ data: Array<string> }>((resolve) => 
        setTimeout(() => resolve({ data: dummyGenres }), 500));
}

const dummyLibary: Array<Book> = [
    { 
        id: 1,
        name: "The Hunger Games", 
        genre: "Action Adventure", 
        author: "Suzanne Collins" 
    },
    { 
        id: 2,
        name: "And Then There Were None", 
        genre: "Mystery", 
        author: "Agatha Christie" 
    },
    { 
        id: 3,
        name: "1984", 
        genre: "Science Fiction", 
        author: "George Orwell" 
    },
];

const dummyGenres: Array<string> = [
    'Action Adventure',
    'Mystery',
    'Science Fiction', 
];
