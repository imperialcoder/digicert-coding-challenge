import { Libary } from './features/libary/Libary';
import './styles/App.css'

function App() {
  return (
    <div className="App">
        <Libary />
    </div>
  );
}

export default App;
